package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"mime/multipart"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	//请求接口地址
	serverUrl := "http://127.0.0.1:8080/alterpic/"
	//全部请求量
	reqNum := 10000
	//并发请求量
	concurrencyReqNum := 2000
	//是否随机用户
	isRandom := true
	//是否POST请求
	isPostMethod := true
	duration := benchmark(serverUrl, reqNum, int32(concurrencyReqNum), isRandom, isPostMethod)
	qps := int(float64(reqNum) / (float64(duration) / 1e9))
	res := fmt.Sprintf("请求接口: %s\t全部请求量: %d\t并发请求量: %d\t是否随机用户: %v\tQPS: %d", serverUrl, reqNum, concurrencyReqNum, isRandom, qps)
	fmt.Println(res)
}

func benchmark(serverUrl string, reqNum int, concurrencyReqNum int32, isRandom bool, isPostMethod bool) (duration time.Duration) {
	reqChannel := make(chan bool)
	// 一组waitGroup代表一组并发请求
	var waitGroup sync.WaitGroup
	waitGroup.Add(int(concurrencyReqNum))
	reqNumTem := int32(reqNum)
	var transport http.RoundTripper = &http.Transport{
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second, // 连接超时
			KeepAlive: 30 * time.Second, // 长连接超时
		}).DialContext,
		MaxIdleConns:          100, // 最大空闲连接
		MaxConnsPerHost:       100,
		IdleConnTimeout:       90 * time.Second, // 空闲连接超时
		TLSHandshakeTimeout:   10 * time.Second, // tls握手时间
		ExpectContinueTimeout: 1 * time.Second,  // 100-continue状态码超时时间
		//	DisableKeepAlives:     true,
	}
	client := &http.Client{
		Transport: transport,
	}
	//一个协程代表一个用户
	reqRoutine := func(i int32) {
		defer waitGroup.Done()
		reqNumRemain := atomic.AddInt32(&reqNumTem, -1)
		for reqNumRemain >= 0 {
			reqNumRemain = atomic.AddInt32(&reqNumTem, -1)

			//拼接username
			username := "username"
			if isRandom {
				rand.Seed(time.Now().Unix())
				username += strconv.Itoa(rand.Intn(10000000))
			} else {
				username += "1"
			}

			////			file~~~~~~~~~~~~~~~~~~
			//extraParams := map[string]string{
			//	"username":   username,
			//	"imagetoken": "testtokeninfo",
			//}
			//picPath := "./src/web/pic/pic1.jpeg"
			//req, err := newfileUploadRequest(serverUrl, extraParams, "image", picPath)

			param := url.Values{}
			//设置请求参数
			param.Add("username", username)
			param.Add("password", "123456")
			param.Add("token", "testtokeninfo")
			param.Add("nickname", "newnick")
			//			param.Add("image", "testImagePath")
			var req *http.Request
			var err error
			if isPostMethod {
				req, err = http.NewRequest("POST", serverUrl, bytes.NewBufferString(param.Encode()))
			} else {
				req, err = http.NewRequest("GET", serverUrl, bytes.NewBufferString(param.Encode()))
			}
			if err != nil {
				fmt.Println(err)
			}
			req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
			//req.AddCookie(&http.Cookie{Name: "username", Value: username, Expires: time.Now().Add(120 * time.Second)})
			//req.AddCookie(&http.Cookie{Name: "token", Value: "test", Expires: time.Now().Add(120 * time.Second)})

			<-reqChannel
			resp, err := client.Do(req)
			if err != nil {
				fmt.Println(err)
			}
			defer resp.Body.Close()
			_, err = ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
	for i := int32(0); i < concurrencyReqNum; i++ {
		go reqRoutine(i)
	}
	close(reqChannel)
	start := time.Now()
	waitGroup.Wait()
	return time.Since(start)
}

// Creates a new file upload http request with optional extra params
func newfileUploadRequest(uri string, params map[string]string, paramName, path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	//fmt.Printf("请求参数：%+v", params)
	part, err := writer.CreateFormFile(paramName, filepath.Base(path))
	if err != nil {
		return nil, err
	}
	_, err = io.Copy(part, file)

	for key, val := range params {
		_ = writer.WriteField(key, val)

	}
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", uri, body)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	return req, err
}
