package rpc

import (
	"bytes"
	"encoding/gob"
	"fmt"
)

// RPC数据传输格式
type RPCdata struct {
	Name string        // 函数名
	Args []interface{} // 请求或应答的参数
	//	Err  string        // 错误信息
}

// 序列化
func Serialize(data RPCdata) ([]byte, error) {
	var buf bytes.Buffer
	encoder := gob.NewEncoder(&buf)
	if err := encoder.Encode(data); err != nil {
		fmt.Println("Serialize failed, error: %v\n", err)
		return nil, err
	}
	return buf.Bytes(), nil
}

// 反序列化
func Deserialize(b []byte) (RPCdata, error) {
	buf := bytes.NewBuffer(b)
	decoder := gob.NewDecoder(buf)
	var data RPCdata
	if err := decoder.Decode(&data); err != nil {
		fmt.Println("Deserialize failed, error: %v\n", err)
		return RPCdata{}, err
	}
	return data, nil
}
