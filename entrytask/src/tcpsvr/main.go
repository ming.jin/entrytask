package main

import (
	"encoding/gob"
	"entrytask/src/protocal"
	"entrytask/src/rpc"
	"entrytask/src/tcpsvr/dao"
	"entrytask/src/tcpsvr/service"
	"fmt"
	_ "net/rpc"
	"os"
	"os/signal"
	"syscall"
)

func init() {
	gob.Register(protocal.LoginReq{})
	gob.Register(protocal.LoginRes{})
	gob.Register(protocal.ShowInfoReq{})
	gob.Register(protocal.ShowInfoRes{})
	gob.Register(protocal.AlterNickReq{})
	gob.Register(protocal.AlterNickRes{})
	gob.Register(protocal.AlterPicReq{})
	gob.Register(protocal.AlterpicRes{})
	gob.Register(protocal.LogoutReq{})
	gob.Register(protocal.User{})
}

func waitElegantExit(c chan os.Signal) {
	for i := range c {
		switch i {
		case syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT:
			// 这里做一些清理操作或者输出相关说明，比如 断开数据库连接
			dao.MysqlClose()
			dao.RedisClose()
			fmt.Println("receive exit signal ", i.String(), ",exit...")
			os.Exit(0)
		}
	}
}

func main() {
	SqlInitErr := dao.InitDB()
	if SqlInitErr != nil {
		return
	}
	RedisInitErr := dao.InitRedis()
	if RedisInitErr != nil {
		return
	}
	RpcServer := rpc.NewRPCServer("0.0.0.0:8081")
	RpcServer.Register("Login", service.Login)
	RpcServer.Register("GetInfo", service.GetUserInfo)
	RpcServer.Register("AlterNick", service.AlterNickname)
	RpcServer.Register("AlterPic", service.AlterPic)
	RpcServer.Register("Logout", service.Logout)
	RpcServer.Run()

	c := make(chan os.Signal)
	// SIGHUP: terminal closed
	// SIGINT: Ctrl+C
	// SIGTERM: program exit
	// SIGQUIT: Ctrl+/
	signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// 阻塞，直到接受到退出信号，才停止进程
	waitElegantExit(c)
}
