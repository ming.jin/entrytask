package util

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

const (
	DEFAULT_ERROR_CODE    int32 = -1
	PARAMETER_ERROR_CODE  int32 = -2
	CREAT_FILE_ERROR      int32 = -3
	UPLOADFILE_ERROR_CODE int32 = -4
)

func JsonResp(objResp http.ResponseWriter, data interface{}, msg string, code int) {
	objResp.Header().Set("Content-type", "application/json")
	objResp.Header().Set("Access-Control-Allow-Origin", "*")
	respMap := map[string]interface{}{"data": data, "msg": msg, "code": code}
	respJson, err := json.Marshal(respMap)
	if err != nil {
		log.Println("WriteJson Err:", err)
		return
	}
	fmt.Println(string(respJson))
	objResp.Write(respJson)
}

func ErrJsonResp(objResp http.ResponseWriter, code int, msg string) {
	objResp.Header().Set("Content-type", "application/json")
	objResp.Header().Set("Access-Control-Allow-Origin", "*")
	respMap := map[string]interface{}{"data": "", "msg": msg, "code": code}
	respJson, err := json.Marshal(respMap)
	if err != nil {
		log.Println("WriteJson Err:", err)
		return
	}
	fmt.Println(string(respJson))
	objResp.Write(respJson)
}
